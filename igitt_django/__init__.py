"""
This django application contains django models to store and reuse IGitt
objects.
"""
from os.path import dirname, join

with open(join(dirname(__file__), 'VERSION'), 'r') as ver:
    VERSION = ver.readline().strip()
